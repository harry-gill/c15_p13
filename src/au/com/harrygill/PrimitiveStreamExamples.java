package au.com.harrygill;

public class PrimitiveStreamExamples {

    public static void main(String[] args) {

        /*Stream<Integer> intStream = Stream.of(1, 2, 3, 4, 5);

        long sum = intStream.reduce(0, (i, j) -> i + j);
        System.out.println("Sum = " + sum);

        long count = intStream.count();

        System.out.println("Average = " + sum / count);*/

        /*IntStream intStream = IntStream.of(1, 2);
        intStream.average().ifPresent(System.out::println);

        LongStream longStream = LongStream.of(1L, 2L);
        longStream.average().ifPresent(System.out::println);

        DoubleStream doubleStream = DoubleStream.of(1.5, 2.5);
        doubleStream.average().ifPresent(System.out::println);*/

        // boxed()
        /*Stream<Integer> integerStream = IntStream.of(1, 2).boxed();
        Stream<Long> longStream = LongStream.of(1, 2).boxed();
        Stream<Double> doubleStream = DoubleStream.of(1, 2).boxed();*/


        //min()
        /*IntStream.of(1, 2).min().ifPresent(System.out::println);
        LongStream.of(1L, 2L).min().ifPresent(System.out::println);
        DoubleStream.of(1D, 2D).min().ifPresent(System.out::println);*/

        // max()
        /*IntStream.of(1, 2).max().ifPresent(System.out::println);
        LongStream.of(1L, 2L).max().ifPresent(System.out::println);
        DoubleStream.of(1D, 2D).max().ifPresent(System.out::println);*/

        //sum()
        /*System.out.println(IntStream.of(1, 2).sum());
        System.out.println(LongStream.of(1L, 2L).sum());
        System.out.println(DoubleStream.of(1D, 2D).sum());*/

        /*System.out.println("Max Integer = " + Integer.MAX_VALUE);
        System.out.println(IntStream.of(Integer.MAX_VALUE, 1).sum());*/


        //range(int, int)
        /*IntStream.range(1, 3).forEach(System.out::println);
        LongStream.range(1L, 3L).forEach(System.out::println);*/

        //rangeClosed(int, int)
        /*IntStream.rangeClosed(1, 3).forEach(System.out::println);
        LongStream.rangeClosed(1L, 3L).forEach(System.out::println);*/

        //summaryStatistics()
        /*System.out.println(IntStream.of(1, 5).summaryStatistics().toString());
        System.out.println(LongStream.of(1L, 5L).summaryStatistics().toString());
        System.out.println(DoubleStream.of(1D, 5D).summaryStatistics().toString());*/
    }
}